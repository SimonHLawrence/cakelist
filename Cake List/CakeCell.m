//
//  CakeCell.m
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//

#import "CakeCell.h"
#import "ImageCache.h"
#import "Cake.h"
#import "CakeServices.h"

@interface CakeCell()

@property (weak, nonatomic) IBOutlet UIImageView *cakeImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation CakeCell

- (UITableView *)tableView
{
    return (UITableView *)[self superview];
}

- (void)present:(Cake *)cake forIndexPath:(NSIndexPath *)indexPath cakeServices:(id<CakeServices>)cakeServices;
{
    self.titleLabel.text = cake.title;
    self.descriptionLabel.text = cake.cakeDescription;
    
    NSURL *imageURL = [NSURL URLWithString:cake.imageURL];
    
    [self applyImageWithURL:imageURL
               forIndexPath:indexPath
               cakeServices:cakeServices];
}

- (void)applyImage:(UIImage *)image data:(NSData *)imageData withURL:(NSURL *)imageURL forIndexPath:(NSIndexPath *)indexPath
{
    if (![[self.tableView indexPathForCell:self] isEqual:indexPath])
        return;
    
    [[self cakeImageView] setImage:image];
    
    if (!image)
        return;
    
    [[ImageCache sharedImageCache] storeImage:imageData
                                      withURL:imageURL];
}

- (void)applyImageWithURL:(NSURL *)imageURL forIndexPath:(NSIndexPath *)indexPath cakeServices:(id<CakeServices>)cakeServices
{
    if (!imageURL) {
        
        [self.cakeImageView setImage:nil];
        return;
    }
    
    UIImage *cachedImage = [[ImageCache sharedImageCache] imageWithURL:imageURL];
    
    if (cachedImage != nil) {
        
        [self.cakeImageView setImage:cachedImage];
        return;
    }
    
    __weak typeof (self) wSelf = self;
    NSIndexPath *pendingIndexPath = [indexPath copy];
    
    [self.cakeImageView setImage:nil];
    
    [cakeServices cakeImageWithURL:imageURL
                        completion:^(UIImage *image, NSData *imageData, NSError *error) {
                            
#ifdef DEBUG
                            if (error) {
                                NSLog(@"%@: %@", [imageURL absoluteString], [error localizedDescription]);
                            }
#endif
                            typeof (self) sSelf = wSelf;
                            
                            if (!sSelf)
                                return;
                            
                            [sSelf applyImage:image
                                         data:imageData
                                      withURL:imageURL
                                 forIndexPath:pendingIndexPath];
                        }];
}

@end
