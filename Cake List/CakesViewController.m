//
//  MasterViewController.m
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//

#import "CakesViewController.h"
#import "CakeCell.h"
#import "CakesController.h"

@interface CakesViewController ()<CakesControllerDelegate>

@property (strong, nonatomic) IBOutlet CakesController *cakesController;

@end

@implementation CakesViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 64.0f;
    
    [self.cakesController refreshCakes];
    
    [self.tableView.refreshControl addTarget:self
                                      action:@selector(refreshData:)
                            forControlEvents:UIControlEventValueChanged];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cakesController numberOfCakes];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CakeCell *cell = (CakeCell *)[tableView dequeueReusableCellWithIdentifier:@"CakeCell"];
    
    Cake *cake = [self.cakesController cakeForIndexPath:indexPath];
    
    [cell present:cake
     forIndexPath:indexPath
     cakeServices:self.cakesController.cakeServices];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
}

#pragma mark - Refreshing

- (void)cakesControllerDidBeginRefreshing:(CakesController *)controller
{
    if (![self.tableView.refreshControl isRefreshing]) {
        
        [self.tableView.refreshControl beginRefreshing];
    }
}

- (void)cakesController:(CakesController *)controller didEndRefreshingWithStatus:(NSString *)status
{
    [self.tableView reloadData];
    [self.tableView.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:status]];
    [self.tableView.refreshControl endRefreshing];
}

- (IBAction)refreshData:(id)sender
{
    if ([self.tableView.refreshControl isRefreshing]) {
        
        [self.cakesController refreshCakes];
    }
}

@end
