//
//  Cake+JSONUpdating.m
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import "Cake+JSONUpdating.h"

@implementation Cake (JSONUpdating)

NSString *const CakeTitleKey = @"title";
NSString *const CakeDescriptionKey = @"desc";
NSString *const CakeImageURLKey = @"image";

- (NSDictionary *)serializeToJSONDictionary:(NSError *__autoreleasing *)error
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    result[CakeTitleKey] = self.title;
    result[CakeDescriptionKey] = self.cakeDescription;
    result[CakeImageURLKey] = self.imageURL;
    
    return result;
}

- (BOOL)updateFromJSONDictionary:(NSDictionary *)dictionary error:(NSError *__autoreleasing *)error
{
    self.title = dictionary[CakeTitleKey];
    self.cakeDescription = dictionary[CakeDescriptionKey];
    self.imageURL = dictionary[CakeImageURLKey];
    
    return YES;
}

@end
