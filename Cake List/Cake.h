//
//  Cake.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cake : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *cakeDescription;
@property (nonatomic, strong) NSString *imageURL;

@end
