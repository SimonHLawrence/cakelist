//
//  CakeServices.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

@import UIKit;
@class Cake;
@protocol NetworkTransport;

typedef void (^CakeImageCompletion) (UIImage *image, NSData *imageData, NSError *error);

typedef void (^CakeServiceCompletion) (NSArray<Cake *> *cakes, NSError *error);

@protocol CakeServices<NSObject>

- (void)cakeImageWithURL:(NSURL *)url completion:(CakeImageCompletion)completion;
- (void)getCakesWithURL:(NSURL *)url completion:(CakeServiceCompletion)completion;

@end

@interface CakeServicesManager : NSObject<CakeServices>

@property (nonatomic, strong) id<NetworkTransport> transport;

@end
