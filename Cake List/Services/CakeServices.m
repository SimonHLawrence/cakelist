//
//  CakeServices.m
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import "CakeServices.h"
#import "DispatchUtilities.h"
#import "Cake.h"
#import "Cake+JSONUpdating.h"
#import "NSURL+SecureURLs.h"
#import "DefaultNetworkTransport.h"

@implementation CakeServicesManager

- (id<NetworkTransport>)transport
{
    if (!_transport) {
        
        _transport = [DefaultNetworkTransport sharedInstance];
    }
    
    return _transport;
}

- (void)cakeImageWithURL:(NSURL *)url completion:(CakeImageCompletion)completion
{
    [self.transport get:[url secureURL]
             completion:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                 
                 UIImage *image = nil;
                 
                 if (data) {
                     
                     image = [UIImage imageWithData:data];
                 }
                 
                 AsyncIfRequired(^{
                     
                     completion(image, data, error);
                 });
             }];
}

- (void)getCakesWithURL:(NSURL *)url completion:(CakeServiceCompletion)completion
{
    [self.transport get:[url secureURL]
             completion:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                 
                 NSArray<Cake *> *cakes = nil;
                 NSArray *cakeDictionaries = nil;
                 
                 if (data) {
                     
                     cakeDictionaries = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:NSJSONReadingAllowFragments
                                                                          error:&error];
                     
                     NSMutableArray<Cake *> *updatedCakes = [NSMutableArray arrayWithCapacity:[cakeDictionaries count]];
                     
                     for (NSDictionary *cakeDictionary in cakeDictionaries) {
                         
                         Cake *cake = [[Cake alloc] init];
                         
                         NSError *updateError = nil;
                         if (![cake updateFromJSONDictionary:cakeDictionary
                                                       error:&updateError]) {
                             
                             AsyncIfRequired(^{
                                 completion(nil, updateError);
                             });
                             
                             return;
                         }
                         
                         [updatedCakes addObject:cake];
                     }
                     
                     cakes = updatedCakes;
                 }
                 
                 AsyncIfRequired(^{
                     
                     completion(cakes, error);
                 });
             }];
}

@end
