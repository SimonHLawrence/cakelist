//
//  DispatchUtilities.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#ifndef DispatchUtilities_h
#define DispatchUtilities_h

#import <Foundation/Foundation.h>

NS_INLINE void AlwaysAsync(dispatch_block_t block) {

    if (!block)
        return;
    
    dispatch_async(dispatch_get_main_queue(), block);
}

NS_INLINE void AsyncIfRequired(dispatch_block_t block) {
    
    if (!block)
        return;
    
    if ([NSThread isMainThread]) {
    
        block();
        return;
    }
    
    AlwaysAsync(block);
}

#endif /* DispatchUtilities_h */
