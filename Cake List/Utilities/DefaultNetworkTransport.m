//
//  DefaultNetworkTransport.m
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import "DefaultNetworkTransport.h"

@interface DefaultNetworkTransport() {
    
    NSURLSession *_session;
}

@end

@implementation DefaultNetworkTransport

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        
        _session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    
    return self;
}

+ (instancetype)sharedInstance
{
    static DefaultNetworkTransport *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        sharedInstance = [[DefaultNetworkTransport alloc] init];
    });
    
    return sharedInstance;
}

- (void)get:(NSURL *)url completion:(NetworkTransportCompletion)completion
{
    
    NSURLSessionDataTask *task = [_session dataTaskWithURL:url
                                         completionHandler:completion];
    [task resume];
}

@end
