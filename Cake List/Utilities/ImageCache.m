//
//  ImageCache.m
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import "ImageCache.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation ImageCache

- (NSString *)cachesPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    return paths[0];
}

- (NSURL *)localURLForImageURL:(NSURL *)imageURL
{
    NSData *imageURLData = [[imageURL absoluteString] dataUsingEncoding:NSUTF8StringEncoding];

    NSMutableData *imageURLHash = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256([imageURLData bytes], (CC_LONG)[imageURLData length], [imageURLHash mutableBytes]);
    
    NSString *imageURLHashString = [[imageURLHash base64EncodedStringWithOptions:0ll] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    return [NSURL fileURLWithPathComponents:@[[self cachesPath], imageURLHashString]];
}

- (UIImage *)imageWithURL:(NSURL *)imageURL
{
    NSURL *localURL = [self localURLForImageURL:imageURL];
    
    NSData *localData = [NSData dataWithContentsOfURL:localURL];
    
    if (!localData) {
        
        return nil;
    }
    
    return [UIImage imageWithData:localData];
}

- (BOOL)storeImage:(NSData *)image withURL:(NSURL *)imageURL
{
    NSParameterAssert(image);
    NSParameterAssert(imageURL);
    
    NSURL *localURL = [self localURLForImageURL:imageURL];
    
    NSError *writeError = nil;
    
    if (![image writeToURL:localURL
           options:NSDataWritingAtomic
                     error:&writeError]) {
    
#ifdef DEBUG
        NSLog(@"Write error: %@", writeError);
#endif
        return NO;
    }
    
    return YES;
}

- (void)purgeImageWithURL:(NSURL *)imageURL
{
    NSURL *localURL = [self localURLForImageURL:imageURL];
    
    [[NSFileManager defaultManager] removeItemAtURL:localURL
                                              error:nil];
}

+ (instancetype)sharedImageCache
{
    static ImageCache *sharedImageCache = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedImageCache = [[ImageCache alloc] init];
    });
    
    return sharedImageCache;
}

@end
