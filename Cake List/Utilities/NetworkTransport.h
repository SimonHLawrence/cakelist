//
//  NetworkTransport.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NetworkTransportCompletion) (NSData *data, NSURLResponse *response, NSError *error);

@protocol NetworkTransport<NSObject>

- (void)get:(NSURL *)url completion:(NetworkTransportCompletion)completion;

@end
