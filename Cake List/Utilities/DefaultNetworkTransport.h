//
//  DefaultNetworkTransport.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkTransport.h"

@interface DefaultNetworkTransport : NSObject<NetworkTransport>

+ (instancetype)sharedInstance;

@end
