//
//  ImageCache.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

@import UIKit;

@interface ImageCache : NSObject

- (UIImage *)imageWithURL:(NSURL *)imageURL;
- (BOOL)storeImage:(NSData *)image withURL:(NSURL *)imageURL;
- (void)purgeImageWithURL:(NSURL *)imageURL;

+ (instancetype)sharedImageCache;

@end
