//
//  NSURL+SecureURLs.m
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import "NSURL+SecureURLs.h"

@implementation NSURL (SecureURLs)

NSString *const NSURLHttpsScheme = @"https";

+ (NSArray<NSString *> *)insecureURLDomains
{
    static NSArray<NSString *> *insecureURLDomains = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        NSDictionary *transportSecurity = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSAppTransportSecurity"];
        
        NSDictionary *insecureDomains = [transportSecurity objectForKey:@"NSExceptionDomains"];
        
        insecureURLDomains = [[insecureDomains allKeys] valueForKey:@"lowercaseString"];
    });
    
    return insecureURLDomains;
}

- (NSURL *)secureURL
{
    if ([[[self scheme] lowercaseString] isEqualToString:NSURLHttpsScheme]) {
        
        return self;
    }
    
    NSURLComponents *components = [NSURLComponents componentsWithURL:self
                                             resolvingAgainstBaseURL:YES];
    
    if ([[NSURL insecureURLDomains] containsObject:[components.host lowercaseString]]) {
        
        return self;
    }
    
    components.scheme = NSURLHttpsScheme;
    return components.URL;
}

@end
