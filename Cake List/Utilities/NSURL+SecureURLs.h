//
//  NSURL+SecureURLs.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (SecureURLs)

- (NSURL *)secureURL;

@end
