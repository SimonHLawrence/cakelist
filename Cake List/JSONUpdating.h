//
//  JSONUpdating.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

@import Foundation;

@protocol JSONUpdating <NSObject>

- (BOOL)updateFromJSONDictionary:(NSDictionary *)dictionary error:(NSError **)error;
- (NSDictionary *)serializeToJSONDictionary:(NSError **)error;

@end
