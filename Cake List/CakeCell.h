//
//  CakeCell.h
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//


@import UIKit;
@class Cake;

@protocol CakeServices;

@interface CakeCell : UITableViewCell

- (void)present:(Cake *)cake forIndexPath:(NSIndexPath *)indexPath cakeServices:(id<CakeServices>)cakeServices;

@end
