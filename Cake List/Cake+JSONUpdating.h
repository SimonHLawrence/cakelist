//
//  Cake+JSONUpdating.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//
#import "Cake.h"
#import "JSONUpdating.h"

@interface Cake (JSONUpdating)<JSONUpdating>

@end
