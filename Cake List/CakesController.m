//
//  CakesController.m
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import "CakesController.h"
#import "CakeServices.h"
#import "Cake.h"
#import "DispatchUtilities.h"

@interface CakesController() {

    NSArray<Cake *> *_cakes;
}

@end

@implementation CakesController

- (Cake *)cakeForIndexPath:(NSIndexPath *)indexPath
{
    return [_cakes objectAtIndex:indexPath.row];
}

- (NSInteger)numberOfCakes
{
    return (_cakes == nil) ? 0 : [_cakes count];
}

- (id<CakeServices>)cakeServices
{
    if (!_cakeServices) {
        
        _cakeServices = [[CakeServicesManager alloc] init];
    }
    
    return _cakeServices;
}

- (NSURL *)cakesURL
{
    if (!_cakesURL) {
        
        _cakesURL = [NSURL URLWithString:@"https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json"];
    }
    
    return _cakesURL;
}

#pragma mark - Refreshing

- (void)refreshCakes
{
    [_delegate cakesControllerDidBeginRefreshing:self];
    
    __weak typeof (self) wSelf = self;
    
    AlwaysAsync(^{
        
        [[self cakeServices] getCakesWithURL:[self cakesURL]
                                  completion:^(NSArray<Cake *> *cakes, NSError *error) {
                                      
                                      typeof (self) sSelf = wSelf;
                                      
                                      [sSelf cakesRefreshed:cakes
                                                      error:error];
                                  }];
    });
}

- (void)cakesRefreshed:(NSArray<Cake *> *)cakes error:(NSError *)error
{
    static NSDateFormatter *shortFormatter = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        shortFormatter = [[NSDateFormatter alloc] init];
        shortFormatter.timeStyle = NSDateFormatterShortStyle;
        shortFormatter.dateStyle = NSDateFormatterShortStyle;
    });
    
    _cakes = cakes;
    
    NSString *status = nil;
    
    if (_cakes) {
        
        status = [NSString stringWithFormat:NSLocalizedString(@"Cakes last updated %@", nil), [shortFormatter stringFromDate:[NSDate date]]];
    }
    else {
        
        status = (error != nil) ? [error localizedDescription] : NSLocalizedString(@"An error occurred updating cakes.", nil);
    }
    
    [_delegate cakesController:self
    didEndRefreshingWithStatus:status];
}

@end
