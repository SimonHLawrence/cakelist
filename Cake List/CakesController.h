//
//  CakesController.h
//  Cake List
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

@import Foundation;
@import UIKit;

@protocol CakeServices;
@class Cake;
@class CakesController;

@protocol CakesControllerDelegate<NSObject>

- (void)cakesControllerDidBeginRefreshing:(CakesController *)controller;
- (void)cakesController:(CakesController *)controller didEndRefreshingWithStatus:(NSString *)status;

@end

@interface CakesController : NSObject

@property (nonatomic, weak) IBOutlet id<CakesControllerDelegate> delegate;

@property (nonatomic, copy) NSURL *cakesURL;
@property (strong, nonatomic) id<CakeServices> cakeServices;

@property (nonatomic, readonly) NSInteger numberOfCakes;
- (Cake *)cakeForIndexPath:(NSIndexPath *)indexPath;

- (void)refreshCakes;

@end
