//
//  CakeServicesTests.m
//  Cake ListTests
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CakeServices.h"
#import "NetworkTransport.h"
#import <UIKit/UIKit.h>

@interface CakeServicesTests : XCTestCase<NetworkTransport>

@property (nonatomic, strong) CakeServicesManager *cakeServices;
@property (nonatomic, weak) XCTestExpectation *networkTransportCalled;
@property (nonatomic, strong) NSURLResponse *networkResponse;
@property (nonatomic, strong) NSData *data;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSURL *dummyURL;

@end

@implementation CakeServicesTests

- (void)setUp
{
    [super setUp];
    
    self.cakeServices = [[CakeServicesManager alloc] init];
    self.cakeServices.transport = self;
    self.dummyURL = [NSURL URLWithString:@"https://www.google.com/cakes"];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testGetCakesSuccess
{
    self.networkTransportCalled = [self expectationWithDescription:@"Network Transport Called"];
    
    XCTestExpectation *cakesRetrieved = [self expectationWithDescription:@"Cakes Retrieved"];
    
    NSArray<NSDictionary *> *sampleCakes = @[@{@"title": @"My First Cake",
                                               @"desc": @"It is a fine cake.",
                                               @"image": @"http:/www.google.com/cakes/cake1.jpeg"},
                                             @{@"title": @"My Second Cake",
                                               @"desc": @"It is another fine cake.",
                                               @"image": @"http:/www.google.com/cakes/cake2.jpeg"},
                                             @{@"title": @"My Third Cake",
                                               @"desc": @"It is yet another fine cake.",
                                               @"image": @"http:/www.google.com/cakes/cake3.jpeg"}];
    
    self.data = [NSJSONSerialization dataWithJSONObject:sampleCakes
                                                options:NSJSONWritingPrettyPrinted
                                                  error:nil];
    self.networkResponse = [[NSHTTPURLResponse alloc] initWithURL:self.dummyURL
                                                       statusCode:200
                                                      HTTPVersion:@"HTTP/1.1"
                                                     headerFields:nil];
    
    [self.cakeServices getCakesWithURL:self.dummyURL
                            completion:^(NSArray<Cake *> *cakes, NSError *error) {
    
                                XCTAssertTrue([NSThread isMainThread], @"Incorrect thread for callback.");
                                XCTAssert([cakes count] == 3, @"Incorrect number of cakes retrieved.");
                                
                                [cakesRetrieved fulfill];
                            }];
    
    [self waitForExpectationsWithTimeout:1.0
                                 handler:^(NSError * _Nullable error) {
                                     
                                 }];
}

- (void)testGetImageSuccess
{
    self.networkTransportCalled = [self expectationWithDescription:@"Network Transport Called"];
    
    XCTestExpectation *imageRetrieved = [self expectationWithDescription:@"Image Retrieved"];
    
    UIImage *cake = [UIImage imageNamed:@"easter-nest-cake.jpg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.data = UIImagePNGRepresentation(cake);
    self.networkResponse = [[NSHTTPURLResponse alloc] initWithURL:self.dummyURL
                                                       statusCode:200
                                                      HTTPVersion:@"HTTP/1.1"
                                                     headerFields:nil];
    [self.cakeServices cakeImageWithURL:self.dummyURL
                             completion:^(UIImage *image, NSData *imageData, NSError *error) {
                                 
                                 XCTAssertTrue([NSThread isMainThread], @"Incorrect thread for callback.");
                                 XCTAssertNotNil(image, @"Image is nil.");
                                 XCTAssertNotNil(imageData, @"Image data is nil.");
                                 
                                 XCTAssertEqualObjects(self.data, imageData, @"Incorrect data retrieved.");
                                 [imageRetrieved fulfill];
                             }];
    
    [self waitForExpectationsWithTimeout:1.0
                                 handler:^(NSError * _Nullable error) {
                                     
                                 }];
}

#pragma mark - NetworkTransport

- (void)get:(NSURL *)url completion:(NetworkTransportCompletion)completion
{
    [self.networkTransportCalled fulfill];
    
    completion(self.data, self.networkResponse, self.error);
}

@end
