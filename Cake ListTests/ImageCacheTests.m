//
//  ImageCacheTests.m
//  Cake ListTests
//
//  Created by Simon Lawrence on 13/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ImageCache.h"

@interface ImageCacheTests : XCTestCase

@property (nonatomic, strong) ImageCache *imageCache;

@end

@implementation ImageCacheTests

- (void)setUp
{
    [super setUp];
    
    self.imageCache = [[ImageCache alloc] init];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testImageCacheSuccess
{
    UIImage *cake = [UIImage imageNamed:@"easter-nest-cake.jpg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    
    NSData *cakeData = UIImagePNGRepresentation(cake);
    
    NSURL *remoteURL = [NSURL URLWithString:@"http://www.google.com/cakey/cake/cake2.jpeg"];
    
    XCTAssertNil([self.imageCache imageWithURL:remoteURL]);
    
    XCTAssertTrue([self.imageCache storeImage:cakeData
                        withURL:remoteURL]);
    
    XCTAssertNotNil([self.imageCache imageWithURL:remoteURL]);
    
    [self.imageCache purgeImageWithURL:remoteURL];
    
    XCTAssertNil([self.imageCache imageWithURL:remoteURL]);
}

- (void)testSharedInstanceExists
{
    XCTAssertNotNil([ImageCache sharedImageCache]);
}
@end
