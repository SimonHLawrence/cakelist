//
//  SecureURLTests.m
//  Cake ListTests
//
//  Created by Simon Lawrence on 13/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSURL+SecureURLs.h"

@interface SecureURLTests : XCTestCase

@end

@implementation SecureURLTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testSchemeTranslation
{
    NSURL *httpURL = [NSURL URLWithString:@"http://www.google.com/"];
    
    NSURL *secureURL = [httpURL secureURL];
    
    XCTAssertEqualObjects([secureURL scheme], @"https");
    
    NSURL *httpsURL = [NSURL URLWithString:@"https://www.google.com"];
    
    XCTAssertEqual(httpsURL, [httpsURL secureURL]);
}

- (void)testExcludedURLsAreExcluded
{
    NSURL *httpURL = [NSURL URLWithString:@"http://www.villageinn.com/derp/"];
    
    NSURL *secureURL = [httpURL secureURL];
    
    XCTAssertEqualObjects([secureURL scheme], @"http");
    XCTAssertEqual(httpURL, secureURL);
}

@end
