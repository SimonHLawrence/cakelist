//
//  CakeJSONTests.m
//  Cake ListTests
//
//  Created by Simon Lawrence on 13/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Cake+JSONUpdating.h"

@interface CakeJSONTests : XCTestCase

@end

@implementation CakeJSONTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testJSONroundTripSuccess
{
    NSDictionary *cakeDictionary = @{@"title": @"My First Cake",
                                     @"desc": @"It is a fine cake.",
                                     @"image": @"http:/www.google.com/cakes/cake1.jpeg"};
    
    Cake *cake = [[Cake alloc] init];
    
    NSError *jsonError = nil;
    XCTAssertTrue([cake updateFromJSONDictionary:cakeDictionary
                             error:&jsonError]);
    XCTAssertNil(jsonError);
    NSDictionary *roundTripDictionary = [cake serializeToJSONDictionary:&jsonError];
    XCTAssertNotNil(roundTripDictionary);
    XCTAssertNil(jsonError);
    XCTAssertEqualObjects(cakeDictionary, roundTripDictionary);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
