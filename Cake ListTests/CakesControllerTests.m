//
//  CakesControllerTests.m
//  Cake ListTests
//
//  Created by Simon Lawrence on 12/07/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CakesController.h"
#import "CakeServices.h"
#import "Cake.h"

@interface CakesControllerTests : XCTestCase<CakeServices, CakesControllerDelegate>

@property (nonatomic, strong) CakesController *cakesController;
@property (nonatomic, weak) XCTestExpectation *getCakesWithURLCalled;
@property (nonatomic, weak) XCTestExpectation *didBeginRefeshingCalled;
@property (nonatomic, weak) XCTestExpectation *didEndRefreshingWithStatusCalled;
@property (nonatomic, strong) NSArray<Cake *> *cakes;

@end

@implementation CakesControllerTests

- (void)setUp
{
    [super setUp];

    self.cakesController = [[CakesController alloc] init];
    self.cakesController.cakeServices = self;
    self.cakesController.delegate = self;
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testRefreshCakesSuccess
{
    self.getCakesWithURLCalled = [self expectationWithDescription:@"getCakesWithURL called"];
    self.didBeginRefeshingCalled = [self expectationWithDescription:@"didBeginRefreshing called"];
    self.didEndRefreshingWithStatusCalled = [self expectationWithDescription:@"didEndRefreshingWithStatus called"];
    
    Cake *cake1 = [[Cake alloc] init];
    cake1.title = @"My Loveley Cake 1";
    cake1.cakeDescription = @"It is a lovely cake.";
    cake1.imageURL = @"http://www.google.com/cake1.jpg";
    
    Cake *cake2 = [[Cake alloc] init];
    cake2.title = @"My Loveley Cake 2";
    cake2.cakeDescription = @"It is a lovely cake again.";
    cake2.imageURL = @"http://www.google.com/cake2.jpg";
    
    Cake *cake3 = [[Cake alloc] init];
    cake3.title = @"My Loveley Cake 3";
    cake3.cakeDescription = @"It is a lovely cake encore une fois.";
    cake3.imageURL = @"http://www.google.com/cake3.jpg";
    
    self.cakes = @[cake1, cake2, cake3];
    
    [self.cakesController refreshCakes];
    
    [self waitForExpectationsWithTimeout:1.0
                                 handler:^(NSError * _Nullable error) {
                                     
                                 }];
    
    XCTAssertEqual(self.cakesController.numberOfCakes, 3, @"Incorrect number of cakes");
    
    for (NSInteger row = 0; row < self.cakesController.numberOfCakes; ++row) {
        
        Cake *refreshedCake = [self.cakesController cakeForIndexPath:[NSIndexPath indexPathForRow:row
                                                                                        inSection:0]];
        
        XCTAssertEqualObjects(refreshedCake.title, self.cakes[row].title, @"Title mutated.");
        XCTAssertEqualObjects(refreshedCake.cakeDescription, self.cakes[row].cakeDescription, @"Description mutated.");
        XCTAssertEqualObjects(refreshedCake.imageURL, self.cakes[row].imageURL, @"Image URL mutated.");
    }
}

#pragma mark - CakeServices

- (void)cakeImageWithURL:(NSURL *)url completion:(CakeImageCompletion)completion
{
    // Unused
}

- (void)getCakesWithURL:(NSURL *)url completion:(CakeServiceCompletion)completion
{
    [self.getCakesWithURLCalled fulfill];
    completion(self.cakes, nil);
}

#pragma mark - CakesControllerDelegate

- (void)cakesController:(CakesController *)controller didEndRefreshingWithStatus:(NSString *)status
{
    [self.didEndRefreshingWithStatusCalled fulfill];
}

- (void)cakesControllerDidBeginRefreshing:(CakesController *)controller
{
    [self.didBeginRefeshingCalled fulfill];
}

@end
